---
title: Office of the Web Minister
subtitle: What do Web Ministers do?
---
<h3>What do web ministers do?</h3>
<p>Aodh, who is the Drachenwald Web Minister, would love to know this. He's largely making it up as he goes along. However, he takes care of the Kingdom Website, and in theory has the people who take care of the various principality and baronial websites report to him.</p>
<p>For a more coherent account of what the Drachenwald Web Team does, you can read about <a href="http://www.drachenwald.sca.org/posts/news/2019/04/04/serving-newcomers-and-existing-members/">the new Drachenwald website</a>, some further updates <a href="http://www.drachenwald.sca.org/posts/news/2019/04/19/building-for-the-future/">three weeks after launch</a>, and the new <a href="http://www.drachenwald.sca.org/posts/news/2019/04/29/this-is-drachenwald/">This is Drachenwald feature</a>. Aodh would like to point out that the credit for an awful lot of this work goes to the other members of the web team; he mostly made confused noises and kept the old website limping along while the work went on.</p>
<h3>Information about reporting</h3>
<p>Mostly, Aodh would like to hear from web ministers. Up to you what to report. You can contact him at <a href="mailto:webminister@drachenwald.sca.org">webminister@drachenwald.sca.org</a>, and he'll be pleased to hear from you.</p>
