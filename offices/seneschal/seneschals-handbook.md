---
title: Seneschal's Handbook
---
<p>Version 2.5. This is the Seneschal's handbook of Drachenwald. It is intended as a guide to the post of Branch Seneschal. The Governing Documents of the SCA Inc. define the SCA, if there is a conflict with those document herein, the Governing Documents have precedence.</p>
<h3>Seneschals' Handbook.</h3>
<p>This is a basic guide to the post of local seneschal for branches below Baronial/Provincial level. It's designed to aid branch seneschals but is not a complete guide to every aspect of the post. If you need clarification or more information on any part of the post please contact the Kingdom Seneschal or one of the Deputy Kingdom Seneschals. That's what they are there for.</p>
<p><strong>What is a Seneschal?</strong>The Seneschal is the chief administrative officer of a branch of the SCA. It is similar to "Club President" in many ways.</p>
<p><strong>Definition of the post:</strong>The post is primarily concerned with the smooth administration of the branch. This includes being the primary conduit for information between the local branch and the larger branch of which it is a part. The Seneschal is the person charged with responsibility for the modern legal affairs of the branch. The Seneschal also is the chief officer of the branch and has a duty to see to the welfare of the branch as a whole.</p>
<p><strong>Scope of the post:</strong>The post of Seneschal covers a large variety of areas. These are probably best summed up as a list of points that the Seneschal must pay attention to. Many of these points will be explained below.</p>
<ol>
<li> Primary contact for legal/governmental authorities for the branch. </li>
<li> Co-ordinating development of the entire branch. </li>
<li> Supporting and continuing liaisons with other branch officers. </li>
<li> Continuing liaisons with the Kingdom Seneschal and/or relevant deputies. </li>
<li> Reporting in a timely and relevant manner. </li>
<li> Helping ensure that all the officers of the branch submit their required reports in a timely manner. </li>
<li> Keeping abreast of developments in the kingdom and relaying this information to the branch and vice-versa. </li>
<li> Working towards the well-being of the branch. </li>
<li> Checking that the branch maintains the minimum requirements for its status and working to help correct any lapse in these requirements. </li>
</ol>
<p><strong>The powers of the post:</strong>The Seneschal has a number of powers that are reserved to the Seneschal.</p>
<ol>
<li> Only the Seneschal has the authority to designate any gathering or function of the branch an "official" function of that branch. This does not mean that the seneschal can dictate how such gatherings shall be run, but rather that the seneschal must sanction them as branch activities. </li>
<li> Any event announcements going to the kingdom newsletter must have the approval of the branch Seneschal attached. This is because all official events are the responsibility of the branch holding them and only the branch Seneschal is authorised to commit the branch to such action. </li>
<li> Whenever possible the Seneschal should be one of the required signatories of any branch bank accounts. </li>
</ol>
<p><strong>Deputies:</strong>The Seneschal of a branch may, and in most cases should, recruit deputies to assist in the running of the office. Deputies may deal with overflow of work or be set to assist with specific tasks. It's recommended that each branch have what is known as an "Emergency" deputy. This is someone who is willing and able to take over the administration of the branch if, for whatever reason (illness, sudden move, etc.), the seneschal is unable to continue to carry out the function of the office. This post is temporary and need not be the same person who is planning to take over from the current Seneschal once that officer's warrant expires. Deputies must be Society Members of the SCA Inc. or any recognized affiliate organisation.</p>
<p><strong>What a seneschal cannot do</strong>While the Seneschal is responsible for overseeing the smooth operation of the shire, the Seneschal must bear in mind that he or she does not have the authority to remove branch Officers. If a problem develops the Seneschal should seek resolution to the established means for redressing a problem that are endorsed by the SCA. These procedures are found at <a href="http://sca.org/docs/welcome.html" title="http://sca.org/docs/welcome.html">http://sca.org/docs/welcome.html</a> under the Governing Documents. It should be noted that if an officer is delinquent in his or her duty over a period of time, then it is the duty of the Seneschal to bring the matter to the attention of the relevant kingdom officer, after first discussing the issues with the delinquent officer.</p>
<h3>General Aspects of the Post of Seneschal</h3>
<p><strong>Reports:</strong>The Seneschal must report to the appropriate Seneschal for their region on a quarterly basis. This report is vitally important, as it is the primary way that you can bring matters of concern to your branch to the attention of the kingdom. It is also important that the kingdom has recourse to information from every branch in the kingdom. This information allows the kingdom to formulate policy to the needs and desires of the people of the kingdom.</p>
<p>As an aid to your office, two weeks before your report is due, a blank report form will be sent to you. If, for any reason you do not receive it, your report is still due by the required date. This report form is designed to be a guide to the minimum information the kingdom is looking for. You should feel free to include any other information or questions that you would like. These reports are currently due on 25 April, 25 July, 25 October, and 25 January, so you should be receiving the form on or about the 11th of these months.</p>
<p>Reports are documents of record. As such they may be made available to anybody who wishes to see them. With this in mind, if you have to bring information of a controversial nature to the attention of your regional Seneschal, you should consider whether it should be in the official report or whether it should be dealt with privately in a separate mail or communication. There is a copy of the report form on page 7 (of the MS Word version) and the on-line form can be found on the Kingdom Seneschal's web pages.</p>
<p>Remember that the Seneschal is there for the benefit of the branch, and as such it is courteous to keep the branch informed as to what is going on. Sending some form of report to the branch's newsletter or e-mail list is often welcome. It should also be suggested that each branch officer do so.</p>
<p><strong>Copies of other officer's reports:</strong> To formulate comprehensive policy for any branch the branch Seneschal must have access to information on the various activities of the branch. To this end the Seneschal should request a copy of the report of each officer in the branch.</p>
<p><strong>Liaisons with local officers</strong>A branch Seneschal should be working closely with the other officers of the branch to achieve the best that they can for the members locally, as well as for the benefit of the larger SCA. It is recommended that each group have regular officer meetings. If a group covering a large area has difficulty in getting officers together on a regular basis for meetings it is worth considering holding discussions via e-mail. Some groups have set up officers' e-mail discussion lists to help with their administration.</p>
<p><strong>Keeping branch listings in newsletters up to date</strong>An important part of the job of Seneschal is making sure that information about the branch is up-to-date. A Seneschal should check that the listings for the branch in the kingdom newsletter, regional newsletter, kingdom web site, any regional web site, and, if there is one, the branch web site or newsletter. These can be located by checking the kingdom website on the Regnum or by contacting your regional Deputy Kingdom Seneschal (see contact section).</p>
<p><strong>Local Branch status requirements:</strong>The Seneschal is responsible for ensuring that the branch continues to meet the minimum requirements for their branch status.These are as follows for Shire, Canton, College, and Stronghold branches:</p>
<ol>
<li> The branch must have at least 5 subscribing members of the SCA Inc. or any recognized affiliate group. </li>
<li> The branch must have a Seneschal </li>
<li> The branch must have an Exchequer </li>
<li> The branch must have at least one of these three officers: a Herald, a Marshal or a Minister of Arts and Sciences. </li>
<li> Each officer of the branch must be a Society Member (one who has paid a membership to the SCA Inc. or any recognised affiliated organisation.) </li>
<li> The reporting requirements of these offices must be met. </li>
</ol>
<p><strong>Branch Finances:</strong>The branch must keep track of its finances and comply with the exchequer policies of the SCA and the Kingdom of Drachenwald. The Kingdom of Drachenwald Exchequer policies are available from your regional exchequer or from the Kingdom Exchequer. It should be noted that the financial reporting requirements of your Country take precedence. If you have any difficulties or questions the Kingdom Exchequer, or one of his/her deputies should be able to help you. (See contact section for details of how to find these officers.) Kingdom Financial Policy and Exchequer Policy should also be available on the kingdom website at the Kingdom Law page</p>
<p><strong>Kingdom officers</strong>Each branch officer reports to the appropriate regional officer. These officers then report to the kingdom officers. If you have difficulty contacting any of the regional deputies you should contact the relevant kingdom officer. A list of the current kingdom officers can be found on the Drachenwald web site, (see contact section at the end of this document.) and in Dragon's Tale, the kingdom newsletter.</p>
<p><strong>Appointment of officers</strong>When the branch has a new officer, the following should be done. The Seneschal of the branch should contact the appropriate Regional and Kingdom officer. This should include a statement from the Seneschal confirming that the branch supports the person applying.</p>
<p>The new officer should contact the kingdom officer too, and include details of their membership and contact information. Normally a copy of the person's membership card showing the expiration date is used. Membership details may also be found on the mailing label from the Dragon's Tale or Tournaments Illuminated; you may send a copy of one of these labels as proof of membership if a membership card is not available.</p>
<p>The person applying for the post, if acceptable to the appropriate Kingdom Officer, will then be warranted for the office. Once they are warranted then they are an officer of the branch.</p>
<p>The seneschal must keep track of the state of the warrants of the other officers of the branch and should assist the new officer in getting warranted if needed. People must be warranted to be officers. If they are not, it opens up an area of difficulty for the group such as the possibility of withdrawal of branch status or problems with events being designated non-official. This is something that can easily be avoided, and I want to be sure that it does not become a problem. If you are having any problem with the warranting process please let the Kingdom Seneschal know. The Kingdom Seneschal can and will help avoid any problems in this.</p>
<p><strong>How official events work:</strong>Once a branch decides that it wishes to host an event, the branch must select a person to be in overall charge of the event. This person, the Event Steward or Autocrat, should then be authorised as a deputy of the branch seneschal with authority for the planning and execution of the event. This officer should keep the branch seneschal informed of the progress on the event.</p>
<p>It is the obligation of the event steward and failing him/her, the seneschal to ensure that each event operates under SCA rules and nothing happens that could endanger the health and safety of the people attending the event. The other officers of the shire share this responsibility, as laid out in corpora II E. If a situation arises which presents a problem of this nature, the, event Steward or seneschal must do whatever they can to stop the problem, up to and including removing Society sanction for the event, and notifying the responsible officers and the owner of the site (or agent) that it is no longer an SCA event. If this happens, the kingdom seneschal must be notified within 24 hours, as there is additional paperwork to be filled out. One should always bear in mind that any modern criminal activity should also be dealt with by the relevant legal authorities.</p>
<p><strong>Insurance</strong>The SCA has an international insurance policy to cover officers and the Society. It is vital that event organisers and officers are properly authorised to avail of what protection this gives.</p>
<p>A statement of insurance (often it is useful to help get sites for events) is available from the SCA's corporate office. See the section on contact addresses below for details of the SCA corporate office.</p>
<p><strong>Society events:</strong>There are two main categories of Society events. Those simply called "Society Events" and those called "Official Events".</p>
<p>A "Society Event" is any gathering of SCA members for an SCA purpose which is approved by Seneschal of the sponsoring branch and which is advertised to the local members. It is an SCA activity and in effect is official. It's just that the Society calls events that require prior publication in the Dragon's Tale "Official Events". Confusing I know, but we are stuck with it.</p>
<p>An "Official Event" is one that is advertised in the Dragon's Tale, so that everyone is aware of it in advance. This allows for official actions to take place at it. Many things need to be done only at official events so that people can find out about them, and in some cases so people can contribute to the issues (such as a Curia Regis, a meeting to change laws of the kingdom). Awards may only be given out at official events and any changes to kingdom law must be made at official events.</p>
<p>To make an event official the Seneschal of the sponsoring branch must approve it and an announcement for the event must be published in the kingdom newsletter in advance of the event. This announcement must include certain information about times and location, etc. These details can be found in the Kingdom Chronicler's Policies.</p>
<p><strong>Kingdom level events:</strong>There are six regular events each year that are referred to as "Kingdom level events". These are Coronation in January, Crown Tournament in March, Kingdom Arts and Science faire in April, Coronation in June, Crown Tournament in October and Kingdom University in November. These events are well attended and bring many people to the area hosting them. As a result there is considerable competition to host them. There is a rotation among the regions for where the next Coronation or Crown Tournament event should be held. Any group in the appointed region may bid to hold the event. Kingdom University and Kingdom Arts and Science fair are not on this rotation schedule and any branch may bid to host them. The bid should detail what your branch can offer by way of site, costs, and accessibility to airports, activities offered, feast, etc.</p>
<p>The Crown and Officers involved will then decide which bid presents the best option and give that branch the permission to host the event. Details of this process are available from the Kingdom Seneschal.</p>
<p>The rotational calendar is published in the Dragon's Tale as a planning aid.</p>
<p><strong>Conduct and Mediatory Duties:</strong>It is very important that a seneschal be able to deal diplomatically with people. Often conflicts arise in any organisation. Usually these come about from misunderstandings. However, in the heat of the moment a simple misunderstanding can flare up into quite an ugly incident.</p>
<p>There are some pointers to bear in mind if a confrontation begins in your branch.</p>
<ol>
<li> Avoid taking sides whenever possible. As seneschal you need to represent the whole branch. </li>
<li> Try to calm things down first. A settlement is less likely if people are angry. </li>
<li> Re-read all mail before you send it. Be sure it's not going to inflame things further. </li>
<li> Always attempt a resolution with the people involved before taking things any further. </li>
<li> Always check with both sides that you've understood them correctly.</li>
</ol>
<p><strong>Democracy:</strong>While the SCA is not a democratic institution, it is advisable for all officers to pay attention to what their branch wants. Unless the branch, its officers, and their policies have support from within the branch, it will be very difficult to make progress.</p>
<p><strong>Governing Documents:</strong>The way that the SCA operates is detailed in its governing documents. These can be found at <a href="http://www.sca.org/docs/welcome.html" title="http://www.sca.org/docs/welcome.html">http://www.sca.org/docs/welcome.html</a></p>
<p>Kingdom Law for Drachenwald can be found at The Kingdom Law page Every seneschal should be familiar with these documents.</p>
<p><strong>Blank Quarterly report details for Local Branches.</strong></p>
<ul>
<li>Quarter number and modern CE date:</li>
<li>Branch name:</li>
<li>SCA Name of Seneschal:</li>
<li>Legal name of Seneschal:</li>
<li>Address:</li>
<li>Telephone:</li>
<li>E-mail address:</li>
<li>Membership Number:</li>
<li>Membership expiry date:</li>
</ul>
<p>f your Branch is an "incipient" branch please list the sponsoring Branch and the Seneschal of that Branch as well.</p>
<ul>
<li>List of Branch officers:</li>
<li>Membership details for each officer including:</li>
<ul>
<li>SCA Name of Officer:</li>
<li>Legal name of Officer:</li>
<li>Address:</li>
<li>Telephone:</li>
<li>E-mail address:</li>
<li>Membership Number:</li>
<li>Membership expiry date:</li>
</ul>
<li>Please point out any changes in the officer listings since last report:</li>
<li>Details of branch activities since last report. (Details of events, practices, planning for future events):</li>
<li>Goals set for the branch:</li>
<li>Any problems your branch is experiencing/ Resolved problems:</li>
<li>Suggestions to Kingdom or Principality level which need addressing, things which would help other local branches:</li>
<li>Media contacts, copies of printed coverage of your branch are requested if available.</li>
</ul>
<p><strong>Annual Domesday Report</strong>There are some items that are needed for the January "Domesday" report only. In addition to the above reporting requirements the annual Doomsday report shall include the following:</p>
<ul>
<li>An estimated breakdown of the number of members and non-member participants of the group</li>
<li>A short financial summary of the group.</li>
<li>Listing of current officers and confirmation of their status as SCA members in good standing.</li>
</ul>
<p>A blank report form for the Doomsday report will be sent to you a couple of weeks in advance of your deadline.</p>
<p><strong>Contacts.</strong>Websites:</p>
<p>Kingdom of Drachenwald:  From the Drachenwald website you should be able to go directly to your regional website and to your branch website if there is one.</p>
<p>SCA Inc.: <a href="http://www.sca.org" title="http://www.sca.org">http://www.sca.org</a></p>
<p><strong>Kingdom Officers:</strong>With the number of officers and regional deputies totalling well over 25 there will be changes almost every month. Therefore the best way to keep up to date is to visit the officers page on the Kingdom website , check the Dragon's Tale (Kingdom Newsletter, available to members), or contact your regional Deputy Kingdom Seneschal for an up to date list.</p>
<p><strong>Corporate Office:</strong>If you need to get insurance certificates, membership forms (also available on line at <a href="http://sca.org/members/welcome.html" title="http://sca.org/members/welcome.html">http://sca.org/members/welcome.html</a>  and in the Kingdom Newsletter), or otherwise contact the SCA Inc. these are the details for the corporate office.</p>
<p>Member Services Office, Society For Creative Anachronism, P.O. Box 360789, Milpitas, CA 95036-0789, USATelephone: +1 800 789 7486 or +1 408 263 9305</p>
<p>If you need any information on officers or SCA in your region please contact one of the following officers:<br />
<strong>Kingdom Seneschal</strong>Contact details and email for the Senschal staff can be found on the Seneschal's page</p>
<p>If you don't fall into any of these regions please contact the Kingdom Seneschal directly.</p>
<hr />
<p>Compiled by Etienne Fevre. Jan 2002. This document was influenced by "Seneschal 101" by Master Terafan Greydragon. I recommend each seneschal also read that document in the library section on <a href="http://www.greydragon.org" title="www.greydragon.org">www.greydragon.org</a></p>
<p>A great debt of gratitude is due to Mistress Jaella of Armida, who read and corrected this document a great many times for me. Thanks!</p>
<p>Thanks also to Baron Tassilo, Master Wolfgang, Lord Lindorm, Lord Frithof, Lord Heinricus, Master Bertrik, and all those who assisted with the project.</p>
<p>This document will be updated regularly and the most up to date version should be available on <a href="http://www.drachenwald.sca.org" title="http://www.drachenwald.sca.org">http://www.drachenwald.sca.org</a>. under the Seneschal's page.</p>
