---
title: The Dragon's Tale permission form
---
<p>If you are submitting any original work (not officer missives or event announcements) for publication in the Dragon's Tale, you must give formal permission for the item to be published.&nbsp; This can be done using the <a href="{{ site.baseurl }}{% link offices/chronicler/files/permission.pdf %}" target="_blank">PDF permission form</a>, which can be mailed, or scanned and emailed to the Chronicler, or you can send an email version.&nbsp; Copy the text below and paste it into your email. Replace the items in [square brackets] with the appropriate names etc.&nbsp; Decide which permissions you are giving (the items marked with # below) and delete any that aren't appropriate.&nbsp; Then email the completed form to <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,39,62,99,104,114,111,110,105,99,108,101,114,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,60,47,97,62));</script></p>
<p>Mark your email with "[DT] Permission for for (description of item)" in the subject.</p>
<p>==== copy everything below here ====</p>
<p>&nbsp;</p>
<p><strong>PUBLICATION PERMISSION FORM</strong></p>
<p>I. [INSERT REAL NAME], being known within the Society for Creative<br />Anachronism as [INSERT SCA NAME], do hereby grant permission for my<br />[ARTICLE/PHOTO/POEM/OTHER] entitled [INSERT ARTICLE TITLE/FILENAME/ETC] to<br />be used as follows (Delete unused options):</p>
<p># Ownership by the publication titled [INSERT NAME OF PUBLICATION] and all<br />copyright granted to that publication (or to: Society for Creative<br />Anachronism, Inc.), which shall determine all future use of the item named<br />above. (Check no other item.)</p>
<p># One-time publication in an issue of [INSERT NAME OF PUBLICATION]</p>
<p># Publication in [INSERT NAME OF PUBLICATION] no more than [NUMBER OF]<br />times (this may be limited in time to printing within [NUMBER OF] years)</p>
<p># Performance at the event called [INSERT NAME OF EVENT]</p>
<p># Posting on the Web page for [INSERT NAME OF GROUP/EVENT/OTHER] </p>
<p>I recognize that persons unknown may link to this site or may use my work<br />without my permission. I shall hold the web page owner or manager harmless<br />from such activity if proper notice appears on the Web page, and I am<br />immediately notified when the link or use is discovered.</p>
<p>If I have checked one of the last four options, I retain all copyright in<br />my work and may grant permission to any other publication or entity to use<br />my work, or may withdraw permission if I choose. Any such withdrawal of<br />permission shall be provided in writing.</p>
<p>I further certify that I am the sole creator of this work, and have not<br />substantially based it upon the work of any other person. If others have<br />contributed to this work, or if I have based this upon the work of any<br />person, their names and addresses (or other contact information) and the<br />name of the original work are:<br />[INSERT NAMES OF OTHER ARTISTS/AUTHORS/CONTRIBUTORS]</p>
<p>Signed,</p>
<p>Name: [INSERT YOUR NAME]</p>
<p>Date: [INSERT THE DATE]</p>
<p>Address: [INSERT YOUR ADDRESS]</p>
 