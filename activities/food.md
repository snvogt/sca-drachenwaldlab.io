---
title: Food
excerpt: From noble tables to our own
---

The Society enjoys the bounty of people who love good food, and enjoy research, and experiment with recipes. One of the joys of the Society is sitting to feast with friends, with fine food, drink and good company.

Newcomers are sometimes surprised that we are not subsisting on pottage and roast joints alone, but have access to recorded menus from the 14th century onward, and can make good educated guesses about diets and preferences from earlier periods. 

Here are a few samples of dishes, and sources, we've enjoyed in Drachenwald.

Sometimes, you __do__ get to roast a whole animal: this campfire cooking is possible at some sites.  

<img src="{{ site.baseurl }}{% link images/food/cooking-raglan-roast.jpg %}" width="400" alt="Cooking a roast at Raglan">{: .align-middle}

Medieval diets were not all meat. Our feasts are often accommodate people who do not eat meat, those who must avoid gluten, and those who cannot eat dairy. A lentil dish, presented at a potluck in the shire of Thamesreach.  

<img src="{{ site.baseurl }}{% link images/food/pottage-lentils.jpg %}" alt="Dish of lentils">{: .align-middle}

Here's an example of a menu prepared for an event in May 2019 in Ireland, [which is gluten-free](https://duninmara.org/posts/2019/05/08/coronet-feast/).   

Novelty food - food that is pretending to be something else was a feature of medieval fine dining. In a very modest example, beef meatballs are 'dressed' to look like hedgehogs, as they were depicted in medieval books. According to medieval bestiaries, hedgehogs carried grapes on their back to feed their young, and so a heraldic hedgehog is typically drawn or painted with grapes sticking to its spines.  

The recipe appears in [Pleyn Delit: Medieval cookery for modern cooks](https://www.amazon.co.uk/Pleyn-Delit-Medieval-Cookery-Modern/dp/0802076327), a very accessible book about medieval cooking, which draws on multiple sources.   

<img src="{{ site.baseurl }}{% link images/food/hedgepigs-with-grapes.jpg %}" width="360" alt="Hedgepigs with grapes">{: .align-middle}

Some resources for starting medieval cooking at home:

* [Medieval cookery](http://medievalcookery.com/), a website that does what it says on the label. Recipes with original wording, and full redactions - the cook's best guess as to how the recipe is supposed to work, with modern quantities provided.

* [Let hem boyle booklist](https://lethemboyle.com/about/books/) is Mistress Eva's blog, specifically her list of medieval cookbooks. It shows the range of research and food styles available and of interest to people in the SCA. 

* Forme of Cury, a late 14th c English recipe book originally written for Richard II's kitchen, and presents the recipes as they were written.
  * [Transcription of Forme of Cury](http://www.pbm.com/~lindahl/foc/)
  * [Digitised version of one of the original copies](https://www.library.manchester.ac.uk/inthebigynnyng/manuscript/ms7/)
  * [Another copy of Forme of Cury digitised by British Library](http://www.bl.uk/manuscripts/Viewer.aspx?ref=add_ms_5016_fs001r#) 
